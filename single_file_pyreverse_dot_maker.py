import subprocess
import os
from abstract_dot_maker import AbstractDotMaker


class SingleFilePyReverseDotMaker(AbstractDotMaker):

    def __init__(self, file_path):
        self.work_dir = os.path.dirname(file_path)
        if self.work_dir == "":
            self.work_dir = os.path.abspath(".")
        self.file = os.path.basename(file_path)
        self.command_option = self.file

    def make_dot_file(self, command_option=""):
        print("Start making dot file.  work_dir:{0} file:{1}"
              .format(self.work_dir, self.file))
        # change current directory to input_file directory
        command = "pyreverse {0}".format(self.file) + command_option
        # change current directory to do for the directory
        print("command:{0}".format(command))
        subprocess.run(command, cwd=self.work_dir, shell=True)

        print("Finished to make dot file.")
