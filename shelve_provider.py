from abstract_dot_provider import DotProvider
from dot_saver import AbstractDotSaver
from shelve_manager import ShelveManager


class ShelveProvider(DotProvider):
    def _factory_method(self) -> AbstractDotSaver:
        db_name = 'shelf_dot'
        key = 'dot'
        dot_saver = ShelveManager(db_name, key)
        return dot_saver
