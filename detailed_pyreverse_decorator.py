from dot_maker_decorator import PyReverseDotMakerDecorator


class DetailedPyReverseDecorator(PyReverseDotMakerDecorator):

    def make_dot_file(self, command_option=""):
        command = command_option + " -ASmy "
        self.dot_maker.make_dot_file(command)
