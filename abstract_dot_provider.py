from abc import ABCMeta, abstractmethod
from dot_saver import AbstractDotSaver


class DotProvider(metaclass=ABCMeta):
    @abstractmethod
    def _factory_method(self) -> AbstractDotSaver:
        pass

    def create_saver(self) -> AbstractDotSaver:
        provider = self._factory_method()
        return provider
