# BCDE321 Assessment2 2020S1 #

This repository is for Assessment2 of Advanced Programming course.

Yutaka Kawakami, Ashish Shrestha

## Components ##
This program need to be installed the below packages.

### pylint ###
   pyreverse.exe is needed to install with PATH environment variables.

### graphviz ###
   dot.exe is needed to install with PATH environment variables.
    or
   ** write your dot.exe path in dot_exe_file_path.txt **

    ex) H:\work\anaconda3\Library\bin\graphviz\dot.exe

Other components are indicated in 'import' statement.

## How to run  ##
This program will start with the below commands.

### Shell Mode ###
    python main.py /s

### Run Doctest ###
    python main.py /d
    * doctest is defined in doctest_text.txt

## Commands and Options ##

**Documented commands (type help <topic>):**

create_diagram  create_dot  exit  help  open_image  quit  save_dot

help create_diagram

        Create image file (classes.png) in current directory from a dot file.
        This action override the last created data(only one data can be saved).

help create_dot

        Create a dot file into current directory from python code or saved data.
        options
        /f file | directory : Create a dot file from python code file(.py) or project directory
        /s                  : Create a dot file from shelve which saved the last time
        /d                  : Create a dot file from database which saved the last time

help save_dot

        Save a dot file.
        This action override the last saved data(only one data can be saved).
        options
        /s : Save to shelve
        /d : Save to database

help open_image

        Open a image file(classes.png) on current directory.
        options
        /w : Windows
        /m : MacOS