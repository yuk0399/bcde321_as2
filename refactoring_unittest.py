import io
import os
import sys
import unittest
from contextlib import contextmanager
from command import ClassDiagramMakerShell


class RefactoringTestMethods(unittest.TestCase):
    @contextmanager
    def captured_output(self):
        new_out, new_err = io.StringIO(), io.StringIO()
        old_out, old_err = sys.stdout, sys.stderr
        try:
            sys.stdout, sys.stderr = new_out, new_err
            yield sys.stdout, sys.stderr
        finally:
            sys.stdout, sys.stderr = old_out, old_err

    def test00_save_dot_with_no_input(self):
        cmd = ClassDiagramMakerShell()
        os.remove(cmd.dot_file_path)

        with self.captured_output() as (out, err):
            cmd.do_save_dot("/s")

        lines = out.getvalue().splitlines()
        self.assertIn("Dot file not found. Use 'create_dot' command before.", lines)

    def test01_create_dot_wrong_param1(self):
        with self.captured_output() as (out, err):
            cmd = ClassDiagramMakerShell()
            cmd.do_create_dot("/f")

        lines = out.getvalue().splitlines()
        self.assertIn("/f option needs file or directory path", lines)

    def test02_create_dot_wrong_param2(self):
        with self.captured_output() as (out, err):
            cmd = ClassDiagramMakerShell()
            cmd.do_create_dot("/n")

        lines = out.getvalue().splitlines()
        self.assertIn("Choose parameter from [ /f file or dir | /d | /s ]", lines)

    def test03_create_dot_from_shelve_with_no_shelve_file(self):
        if os.path.exists("shelf_dot.db"):
            os.remove("shelf_dot.db")
        cmd = ClassDiagramMakerShell()
        with self.assertRaises(FileNotFoundError):
            cmd.do_create_dot("/s")

    def test04_create_dot_from_database_with_no_db_file(self):
        if os.path.exists("sqlite_dot.db"):
            os.remove("sqlite_dot.db")
        cmd = ClassDiagramMakerShell()
        with self.assertRaises(FileNotFoundError):
            cmd.do_create_dot("/d")

    def test05_create_dot_from_file(self):
        with self.captured_output() as (out, err):
            cmd = ClassDiagramMakerShell()
            cmd.do_create_dot("/f dot_file_manager.py")

        lines = out.getvalue().splitlines()
        self.assertIn("Finished to make dot file.", lines)

    def test06_save_dot_with_wrong_param(self):
        cmd = ClassDiagramMakerShell()
        with self.captured_output() as (out, err):
            cmd.do_save_dot("/m")

        lines = out.getvalue().splitlines()
        self.assertIn("Wrong argument is inputted. Use /s or /d", lines)

    def test07_save_dot_to_shelve(self):
        cmd = ClassDiagramMakerShell()
        with self.captured_output() as (out, err):
            cmd.do_save_dot("/s")

        lines = out.getvalue().splitlines()
        self.assertIn("Success to save dot file to shelve.", lines)

    def test08_save_dot_to_database(self):
        cmd = ClassDiagramMakerShell()
        with self.captured_output() as (out, err):
            cmd.do_save_dot("/d")

        lines = out.getvalue().splitlines()
        self.assertIn("Saved the dot file data", lines)

    def test08_save_dot_to_database2(self):
        cmd = ClassDiagramMakerShell()
        with self.captured_output() as (out, err):
            cmd.do_save_dot("/d")

        lines = out.getvalue().splitlines()
        self.assertIn("Saved the dot file data", lines)

    def test09_create_dot_from_shelve(self):
        cmd = ClassDiagramMakerShell()
        cmd.do_save_dot("/s")
        with self.captured_output() as (out, err):
            cmd.do_create_dot("/s")

        lines = out.getvalue().splitlines()
        self.assertIn("Created a dot file", lines)

    def test10_create_dot_from_database(self):
        with self.captured_output() as (out, err):
            cmd = ClassDiagramMakerShell()
            cmd.do_create_dot("/d")

        lines = out.getvalue().splitlines()
        self.assertIn("Created a dot file", lines)

    def test11_create_dot_from_folder(self):
        with self.captured_output() as (out, err):
            cmd = ClassDiagramMakerShell()
            cmd.do_create_dot("/f .")

        lines = out.getvalue().splitlines()
        self.assertIn("Finished to make dot file.", lines)

