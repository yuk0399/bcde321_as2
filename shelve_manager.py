import os
import shelve
from dot_saver import AbstractDotSaver


class ShelveManager(AbstractDotSaver):
    """
    ShelveManager can save only one file so far.
    """
    def __init__(self, db_name: str, key: str):
        self.db_name = db_name
        self.key = key

    def save_dot_file(self, context):
        assert (context is not None and context != "")

        shelve_instanced = shelve.open(self.db_name, flag='c')
        try:
            shelve_instanced[self.key] = context
            print("Success to save dot file to shelve.")
        finally:
            shelve_instanced.close()

    def read_dot_file(self):

        if not os.path.exists(self.db_name + ".db"):
            raise FileNotFoundError()

        shelve_instanced = shelve.open(self.db_name, flag='r')
        try:
            context = shelve_instanced[self.key]
        finally:
            shelve_instanced.close()

        return context
