import sys
import getopt
from command import ClassDiagramMakerShell


def main(argv):
    try:
        opts, args = getopt.getopt(argv, "hsd", ["help", "shell", "doctest"])
    except getopt.GetoptError as err:
        print(err)
        sys.exit(2)

    for opt, arg in opts:
        if opt in ("-h", "--help"):
            print("usage: python main.py [option]\n"
                  "Options and arguments:\n"
                  "-s          : start shell command\n"
                  "-d          : run doctest code in doctest_text.txt\n")
            sys.exit(0)
        elif opt in ("-s", "--shell"):
            ClassDiagramMakerShell().cmdloop()
            sys.exit(0)
        elif opt in ("-d", "--doctest"):
            import doctest
            doctest.testfile('doctest_text.txt', verbose=True)


if __name__ == '__main__':
    if len(sys.argv) <= 1:
        print("Please input arguments. You can see help with -h")
        sys.exit(2)

    main(sys.argv[1:])
