from abc import ABCMeta, abstractmethod


class AbstractDotSaver(metaclass=ABCMeta):
    @abstractmethod
    def save_dot_file(self, context):
        pass

    @abstractmethod
    def read_dot_file(self):
        pass
