from abc import ABCMeta, abstractmethod


class AbstractImageConverter(metaclass=ABCMeta):
    @abstractmethod
    def make_image_file(self, dot_file_path):
        pass
