from dot_maker_decorator import PyReverseDotMakerDecorator


class IgnoreNonUsableFilesPyReverseDecorator(PyReverseDotMakerDecorator):

    def make_dot_file(self, command_option=""):
        command = command_option + " --ignore=__pycache__ --ignore=.git "
        self.dot_maker.make_dot_file(command)
