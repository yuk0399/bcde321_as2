import os


class DotFileManager:

    def __init__(self, dot_file_path: str):
        self.dot_file_path = dot_file_path

    def read(self):
        assert (os.path.exists(self.dot_file_path))
        with open(self.dot_file_path) as f:
            context = f.read()

        return context

    def write(self, context):
        # write to file (overwrite or make new)
        with open(self.dot_file_path, mode='w') as f:
            f.write(context)
        print("Created a dot file")
