import subprocess
import os
import shutil
from abstract_dot_maker import AbstractDotMaker


class FolderPyReverseDotMaker(AbstractDotMaker):
    def __init__(self, file_path):
        self.work_dir = file_path
        self.file = "*.py"
        self.command_option = self.file

    def make_dot_file(self, command_option=""):
        print("Start making dot file.  work_dir:{0} file:{1}"
              .format(self.work_dir, self.file))
        # change current directory to input_file directory
        command = "pyreverse {0}".format(self.file) + command_option
        # change current directory to do for the directory
        print("command:{0}".format(command))
        subprocess.run(command, cwd=self.work_dir, shell=True)

        # copy to program base folder
        base_path = os.path.join(os.path.abspath("."), "classes.dot")
        output_path = os.path.join(self.work_dir, "classes.dot")
        shutil.move(output_path, base_path)

        print("Finished to make dot file.")
