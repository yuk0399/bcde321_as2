

class TestClass:
    #user2 = User("test")

    def __init__(self, variable: str, value: int):
        self.variable = variable
        self.user = User(value)
        self.user.get_value()

    def test_method(self):
        return self.variable


class User:

    def __init__(self, variable):
        self.variable = variable

    def get_value(self):
        return self.variable
