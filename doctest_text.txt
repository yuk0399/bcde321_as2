# pyreverse_dot_maker module
# ==========================

>>> from pyreverse_dot_maker import SingleFilePyReverseDotMaker
>>> pyreverse_dot_maker = SingleFilePyReverseDotMaker("dot_file_manager.py")
>>> pyreverse_dot_maker.make_dot_file()
Finished to make dot file.

# shelve_manager module
# ==========================

>>> from shelve_manager import ShelveManager
>>> shelve_manager = ShelveManager()
>>> shelve_manager.save_dot_file("This is test.")
Success to save dot file to shelve.
>>> shelve_manager.read_dot_file()
'This is test.'

# sqlite_manager module
# ==========================

>>> from sqlite_manager import SqliteManager
>>> sqlite_manager = SqliteManager()
>>> sqlite_manager.save_dot_file("This is test.")
Saved the dot file data
>>> sqlite_manager.read_dot_file()
'This is test.'

# dot_file_manager module
# ==========================

>>> from dot_file_manager import DotFileManager
>>> import os
>>> dot_file_path = os.path.join(os.path.abspath("."), "\classes.dot")
>>> dot_file_manager = DotFileManager(dot_file_path)
>>> dot_file_manager.write("This is test.")
Created a dot file
>>> dot_file_manager.read()
'This is test.'

# GraphvizImageConverter
# ==========================
>>> from graphviz_image_converter import GraphvizImageConverter
>>> image_converter = GraphvizImageConverter()
>>> image_converter.make_image_file(dot_file_path)
Finished to make image file.

