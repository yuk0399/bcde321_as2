from abstract_dot_provider import DotProvider
from dot_saver import AbstractDotSaver
from sqlite_manager import SqliteManager


class SqliteProvider(DotProvider):
    def _factory_method(self) -> AbstractDotSaver:
        db_name = "sqlite_dot.db"
        dot_saver = SqliteManager(db_name)
        return dot_saver
