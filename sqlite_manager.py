import os
import sqlite3

from dot_saver import AbstractDotSaver


class SqliteManager(AbstractDotSaver):

    def __init__(self, db_name: str):
        self.db_name = db_name

    def _create_connection(self):
        connection = sqlite3.connect(self.db_name)
        return connection

    def _create_table(self, connection, exist: bool):
        cursor = connection.cursor()

        if exist:
            cursor.execute("""DROP TABLE dot_file;""")

        sql_command = """
        CREATE TABLE dot_file (
        context VARCHAR(2048)
        );"""
        cursor.execute(sql_command)
        connection.commit()

    def save_dot_file(self, context):
        assert (context is not None and context != "")
        try:
            db_path = os.path.join(os.path.abspath("."), self.db_name)
            exist = os.path.exists(db_path)

            connection = self._create_connection()
            self._create_table(connection, exist)

            cursor = connection.cursor()
            format_str = """INSERT INTO dot_file (context)
                            VALUES ('{context}');"""

            sql_command = format_str.format(context=context)
            cursor.execute(sql_command)

            connection.commit()
            print("Saved the dot file data")
        finally:
            connection.close()

    def read_dot_file(self):

        if not os.path.exists(self.db_name):
            raise FileNotFoundError()

        try:
            connection = self._create_connection()
            assert (connection is not None)

            cursor = connection.cursor()
            result = cursor.execute("SELECT * FROM dot_file").fetchone()
        finally:
            connection.close()

        return result[0]
