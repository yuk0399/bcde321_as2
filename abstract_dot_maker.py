from abc import ABCMeta, abstractmethod


class AbstractDotMaker(metaclass=ABCMeta):
    @abstractmethod
    def make_dot_file(self, command_option):
        pass

