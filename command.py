from cmd import Cmd
import os
import sys
import shlex
import subprocess

from dot_file_manager import DotFileManager
from graphviz_image_converter import GraphvizImageConverter
from single_file_pyreverse_dot_maker import SingleFilePyReverseDotMaker
from detailed_pyreverse_decorator import DetailedPyReverseDecorator
from ignore_pyreverse_decorator import IgnoreNonUsableFilesPyReverseDecorator
from folder_pyreverse_dot_maker import FolderPyReverseDotMaker
from shelve_provider import ShelveProvider
from sqlite_provider import SqliteProvider


class ClassDiagramMakerShell(Cmd):
    intro = 'Welcome to the shell. Type help or ? to list commands.\n'
    prompt = ">>>"
    dot_file_path = os.path.join(os.path.abspath("."), "classes.dot")
    dot_file_manager = DotFileManager(dot_file_path)

    def __init__(self):
        Cmd.__init__(self)

    def do_create_dot(self, arg):
        """
        Create a dot file in current directory from python code or saved data.
        options
        /f file | dir : Create from python code file(.py) or project directory
        /s            : Create from shelve which saved the last time
        /d            : Create from database which saved the last time
        """
        if arg.startswith("/f"):
            args = arg.split(" ")
            if len(args) != 2:
                print("/f option needs file or directory path")
                return
            file_path = args[1]
            self.make_dot_from_file(file_path)
        elif arg in ["/s", "/d"]:
            if arg == "/s":
                dot_saver = ShelveProvider().create_saver()
            else:
                dot_saver = SqliteProvider().create_saver()

            context = dot_saver.read_dot_file()

            if context is None or context == "":
                print("Dot file is not saved.")
                return

            self.dot_file_manager.write(context)
        else:
            print("Choose parameter from [ /f file or dir | /d | /s ]")
            return

    def make_dot_from_file(self, file_path):
        message = self.check_file(file_path)
        if not message == "":
            print(message)
            return

        if os.path.isfile(file_path):
            # the target is single file
            dot_file_maker = DetailedPyReverseDecorator(
                              SingleFilePyReverseDotMaker(file_path))
        else:
            # the target is folder
            dot_file_maker = IgnoreNonUsableFilesPyReverseDecorator(
                               FolderPyReverseDotMaker(file_path))

        # make dot file
        dot_file_maker.make_dot_file()

    def do_open_image(self, arg):
        """
        Open a image file(classes.png) on current directory.
        options
        /w : Windows
        /m : MacOS
        """
        if arg == "/w":
            subprocess.call(shlex.split("explorer classes.png"))
        elif arg == "/m":
            subprocess.call(shlex.split("open classes.png"))
        else:
            print("Wrong argument is inputted. Use /w (windows) or /m (mac)")

    def do_save_dot(self, arg):
        """
        Save a dot file.
        This action override the last saved data(only one data can be saved).
        options
        /s : Save to shelve
        /d : Save to database
        """
        if arg == "/s":
            # save to shelve
            dot_saver = ShelveProvider().create_saver()
        elif arg == "/d":
            # save to database
            dot_saver = SqliteProvider().create_saver()
        else:
            print("Wrong argument is inputted. Use /s or /d")
            return

        if os.path.exists(self.dot_file_path):
            # read context from dot file
            context = self.dot_file_manager.read()
            dot_saver.save_dot_file(context)
        else:
            print("Dot file not found. Use 'create_dot' command before.")

    def do_create_diagram(self, arg):
        """
        Create image file (classes.png) in current directory from a dot file.
        This action override the last created data(only one data can be saved).
        """
        image_converter = GraphvizImageConverter()
        image_converter.make_image_file(self.dot_file_path)

    def check_file(self, file_path):
        if len(file_path) <= 0:
            return "Please input existing path."

        # check path
        if not os.path.exists(file_path):
            return "File not found. Please check the file/directory."

        # check extension
        if os.path.isfile(file_path):
            root, ext = os.path.splitext(file_path)
            if ext != ".py":
                return "Please input python file(*.py)."

        return ""

    def do_quit(self, arg):
        sys.exit(0)

    def help_quit(self):
        print('Quit this command')

    def help_exit(self):
        print('Quit this command')

    do_exit = do_quit
