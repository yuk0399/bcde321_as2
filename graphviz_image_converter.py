import shlex
import subprocess
import os

from abstract_image_converter import AbstractImageConverter


class GraphvizImageConverter(AbstractImageConverter):
    config_file = "dot_exe_file_path.txt"

    def __init__(self):
        self.dot_exe_file_path = self._get_dot_exe_file_path()

    def _get_dot_exe_file_path(self):
        if not os.path.exists(self.config_file):
            return "dot"

        with open(self.config_file) as f:
            context = f.read().replace('\n', '')
        return context

    def make_image_file(self, dot_file_path):
        assert (os.path.exists(dot_file_path))

        # get dir path from input_file
        work_dir = os.path.dirname(dot_file_path)

        command = '"{0}" -Tpng "{1}" -o classes.png'\
            .format(self.dot_exe_file_path, dot_file_path)
        subprocess.call(shlex.split(command))

        print("Finished to make image file.")
