from abstract_dot_maker import AbstractDotMaker


class PyReverseDotMakerDecorator(AbstractDotMaker):

    def __init__(self, dot_maker: AbstractDotMaker):
        self.dot_maker = dot_maker

    def make_dot_file(self, command_option=""):
        pass
